# Simple Movie App

## Assignment — Flutter Developer (Mobile)
Develop a simple CRUD app for the following model (use dummy data in memory, no need Sqlite/backend):
Movie
• id (PK)
• title
• director
• summary
• tags (can select multiple from e.g. dropdown: Action, Comedy, Fantasy, Horror, Sci-Fi)
Please refer to the attached screen design for illustration.
Required libraries:
• [MobX](https://mobx.netlify.app/)
• [AutoRoute](https://autoroute.vercel.app/)
Success criteria:
1. Share the source code in GitLab (set to public, or invite @thomas.wiradikusuma). 
2. Ensure that it's built as per spec.
3. Ensure that it’s runnable in device/emulator.
4. You will demo it to the interviewer. Please be prepared.
